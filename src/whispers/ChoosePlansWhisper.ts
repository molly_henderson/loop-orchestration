import { whisper } from '@oliveai/ldk';
import { Component, Whisper, WhisperHandler } from '@oliveai/ldk/dist/whisper';
import savePlans from '../aptitudes/vault/savePlans';

type Plan = {
  [key: string]: string | undefined;
};
interface Props {
  plans: Plan[];
  selectedPlans: Plan[];
  myPlans: Plan[];
}
export default class ChoosePlansWhisper {
  whisper: whisper.Whisper;

  label: string;

  props: Props;

  constructor(plans: Plan[]) {
    this.whisper = undefined;
    this.label = 'Choose Plans';
    this.props = {
      plans,
      selectedPlans: [],
      myPlans: [],
    };
  }

  createComponents(): Component[] {
    const components = [];
    this.props.plans.forEach((plan) => {
      components.push({
        type: whisper.WhisperComponentType.Checkbox,
        label: plan.name,
        value: false,
        onChange: (error: Error | undefined, val: boolean) => {
          if (val) {
            this.props.selectedPlans.push(plan);
          } else {
            this.props.selectedPlans.filter((p) => p.id !== plan.id);
          }
        },
      });
    });
    components.unshift({
      type: whisper.WhisperComponentType.Checkbox,
      label: 'Select All',
      value: false,
      onChange: (error: Error | undefined, val: boolean) => {
        components.forEach((c) => (c.value = val));
        this.whisper.update({
          label: this.label,
          components,
        });
      },
    });
    components.push({
      type: whisper.WhisperComponentType.Button,
      label: 'Save',
      onClick: async () => {
        this.props.myPlans = this.props.selectedPlans;
        const x = this.props.myPlans.map((p) => {
          return p.id;
        });
        savePlans.savePlans(x);
        this.close();
      },
    });
    return [
      {
        type: whisper.WhisperComponentType.Message,
        body: 'Select plans to follow',
      },
      {
        type: whisper.WhisperComponentType.CollapseBox,
        children: components,
        open: false,
      },
    ];
  }

  show() {
    whisper
      .create({
        components: this.createComponents(),
        label: this.label,
        onClose: ChoosePlansWhisper.onClose,
      })
      .then((newWhisper) => {
        this.whisper = newWhisper;
      });
  }

  close() {
    this.whisper.close(ChoosePlansWhisper.onClose);
  }

  static onClose(err?: Error) {
    if (err) {
      console.error('There was an error closing Choose Orgs whisper', err);
    }
    console.log('Choose Orgs whisper closed');
  }
}
