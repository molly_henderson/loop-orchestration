import { whisper } from '@oliveai/ldk';
import { Component, Urgency } from '@oliveai/ldk/dist/whisper';

interface Plan {
  [key: string]: any;
}
interface Props {
  plans: Plan[];
}
export default class PlanStatusWhisper {
  whisper: whisper.Whisper;

  label: string;

  props: Props;

  constructor(plans: Plan[]) {
    this.whisper = undefined;
    this.label = 'Your Plan Statuses';
    this.props = {
      plans,
    };
  }

  createComponents(): Component[] {
    const components = this.props.plans.map((plan): Component => {
      const planId = plan.plan?.id;
      const planName = plan.plan?.name;
      const orgId = plan.plan?.organization.id;
      const executions = plan.executions?.slice(0, 5) ?? [];
      const children = executions.map(execution => {
        const start = new Date(execution.startDate).toUTCString();
        const end = execution.stopDate ? new Date(execution.stopDate).toUTCString() : 'now';
        return {
          type: whisper.WhisperComponentType.Link,
          href: `https://orchestrationdev.oliveai.com/plans/${orgId}/${planId}/${execution.executionArn}/run`,
          text: `${execution.status} (${start} - ${end})`,
          style:
            execution.status === 'RUNNING' ? Urgency.Warning :
              execution.status === 'SUCCEEDED' ? Urgency.Success :
                execution.status === 'ABORTED' ? Urgency.Error :
                  execution.status === 'FAILED' ? Urgency.Error :
                    Urgency.None,
        }
      });
      return {
        type: whisper.WhisperComponentType.CollapseBox,
        label: planName,
        open: false,
        children,
      }
    });
    return components;
  }

  show() {
    whisper
      .create({
        components: this.createComponents(),
        label: this.label,
        onClose: PlanStatusWhisper.onClose,
      })
      .then((newWhisper) => {
        this.whisper = newWhisper;
      });
  }

  close() {
    this.whisper.close(PlanStatusWhisper.onClose);
  }

  static onClose(err?: Error) {
    if (err) {
      console.error('There was an error closing Ui whisper', err);
    }
    console.log('Ui whisper closed');
  }
}
