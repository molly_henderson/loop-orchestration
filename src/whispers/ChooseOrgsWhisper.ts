import { whisper } from '@oliveai/ldk';
import { Component, Whisper, WhisperHandler } from '@oliveai/ldk/dist/whisper';
import choosePlans from '../aptitudes/choosePlans/choosePlans';

type Organization = {
  [key: string]: string;
};
interface Props {
  jwt: string;
  organizations: Organization[];
  selectedOrgs: Organization[];
  myOrgs: Organization[];
}

export default class ChooseOrgsWhisper {
  whisper: whisper.Whisper;

  label: string;

  props: Props;

  constructor(organizations: Organization[], jwt: string) {
    this.whisper = undefined;
    this.label = 'Choose Organizations';
    this.props = {
      jwt,
      organizations,
      selectedOrgs: [],
      myOrgs: [],
    };
  }

  createComponents(): Component[] {
    const components = [];
    this.props.organizations.forEach((organization) => {
      components.push({
        type: whisper.WhisperComponentType.Checkbox,
        label: organization.name,
        value: false,
        onChange: (error: Error | undefined, val: boolean) => {
          if (val) {
            this.props.selectedOrgs.push(organization);
          } else {
            this.props.selectedOrgs.filter((o) => o.id !== organization.id);
          }
        },
      });
    });
    components.push({
      type: whisper.WhisperComponentType.Button,
      label: 'Save',
      onClick: () => {
        console.log(
          'Chosen orgIds:',
          this.props.selectedOrgs.map((o) => o.id)
        );
        choosePlans.run(
          this.props.selectedOrgs.map((o) => o.id),
          this.props.jwt
        );
        this.close();
      },
    });
    return [
      {
        type: whisper.WhisperComponentType.Message,
        body: 'Select organizations to follow',
      },
      {
        type: whisper.WhisperComponentType.CollapseBox,
        children: components,
        open: false,
      },
    ];
  }

  show() {
    whisper
      .create({
        components: this.createComponents(),
        label: this.label,
        onClose: ChooseOrgsWhisper.onClose,
      })
      .then((newWhisper) => {
        this.whisper = newWhisper;
      });
  }

  close() {
    this.whisper.close(ChooseOrgsWhisper.onClose);
  }

  static onClose(err?: Error) {
    if (err) {
      console.error('There was an error closing Choose Orgs whisper', err);
    }
    console.log('Choose Orgs whisper closed');
  }
}
