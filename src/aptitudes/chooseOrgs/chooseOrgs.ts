import { network } from '@oliveai/ldk';
import { ChooseOrgsWhisper } from '../../whispers';

const run = async (jwt: string) => {
  const request: network.HTTPRequest = {
    method: 'GET',
    url: 'https://orchestrationdev.oliveai.com/manager/api/public/organizations',
    headers: {
      'x-olive-auth': [jwt],
    },
  };

  const response = await network.httpRequest(request);
  const decodedBody = await network.decode(response.body);
  const orgs = JSON.parse(decodedBody);

  const whisper = new ChooseOrgsWhisper(orgs, jwt);
  whisper.show();
};

export default { run };
