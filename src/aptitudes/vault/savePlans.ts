import { vault } from '@oliveai/ldk';

const savePlans = async (plans: string[]): Promise<boolean> =>
  new Promise((resolve, reject) => {
    const plansString = plans.join(',');
    console.log('Plans:', plansString);
    vault.write('plans', plansString).then(() => {
      console.log('Wrote plans');
      vault
        .exists('plans')
        .then((exists) => {
          console.log(`Value exists in vault: ${exists}`);
          if (!exists) {
            console.log('Value does not exist in vault');
            reject(new Error('Key does not exist in storage'));
            return null;
          }
          return vault.read('plans');
        })
        .catch((error) => {
          reject(error);
        });
    });
  });

export default { savePlans };
