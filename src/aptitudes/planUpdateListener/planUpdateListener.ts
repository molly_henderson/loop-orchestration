import { ui } from '@oliveai/ldk';
import chooseOrgs from '../chooseOrgs/chooseOrgs';

let jwt;

const handler = async (text: string) => {
  if (text.includes('update') || text.includes('plan') || text.includes('choose') || text.includes('list')) {
    chooseOrgs.run(jwt);
  }
};
const listen = (myJwt: string) => {
  jwt = myJwt;
  ui.listenGlobalSearch(handler);
  ui.listenSearchbar(handler);
};

export { handler };
export default { listen };
