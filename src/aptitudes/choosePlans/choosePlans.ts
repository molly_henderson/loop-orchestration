import { network } from '@oliveai/ldk';
import { ChoosePlansWhisper } from '../../whispers';

const run = async (orgIds: string[], jwt: string) => {
  let plans = [];
  await Promise.all(
    orgIds.map(async (orgId) => {
      const request: network.HTTPRequest = {
        method: 'GET',
        url: `https://orchestrationdev.oliveai.com/manager/api/public/plans?organizationId=${orgId}`,
        headers: {
          'x-olive-auth': [jwt],
        },
      };

      const response = await network.httpRequest(request);
      const decodedBody = await network.decode(response.body);
      plans = plans.concat(JSON.parse(decodedBody));
    })
  );

  const whisper = new ChoosePlansWhisper(plans);
  whisper.show();
};

export default { run };
