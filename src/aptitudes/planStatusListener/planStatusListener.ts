import { network, ui, vault } from '@oliveai/ldk';
import { PlanStatusWhisper } from '../../whispers';

let jwt;

const requestPlanStatus = async (planIds: string[], jwt): Promise<Record<string, any>[]> => {
  let plans = [];
  await Promise.all(planIds.map(async planId => {
    const request: network.HTTPRequest = {
      method: 'GET',
      url: `https://orchestrationdev.oliveai.com/manager/api/plans/${planId}/executions`,
      headers: {
        'x-olive-auth': [jwt],
      }
    }

    const response = await network.httpRequest(request);
    const decodedBody = await network.decode(response.body);
    plans = plans.concat(JSON.parse(decodedBody));
  }));

  return plans;
}

const handler = async (text: string) => {
  if (text.includes('status') || text.includes('plan') || text.includes('update')) {
    const planIds = (await vault.read('plans') || '').split(',');
    const planStatuses = await requestPlanStatus(planIds, jwt);
    const whisper = new PlanStatusWhisper(planStatuses);
    whisper.show();
  }
};
const listen = (myJwt: string) => {
  jwt = myJwt;
  ui.listenGlobalSearch(handler);
  ui.listenSearchbar(handler);
};

export { handler };
export default { listen };
