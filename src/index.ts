import {
  chooseOrgs,
  planStatusListener,
  planUpdateListener,
} from './aptitudes';

const jwt = '<your-dev-jwt-here>';
chooseOrgs.run(jwt);
planUpdateListener.listen(jwt);
planStatusListener.listen(jwt);
